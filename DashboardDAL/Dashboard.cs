﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DashboardDAL
{
    public class Dashboard
    {

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Definition { get; set; }
        public string Type { get; set; }
        public bool Active { get; set; }
        public int Source { get; set; }
        public bool AllowExport { get; set; }

        public List<Dashboard> GetByType(string Type)
        {
            var ListToReturn = new List<Dashboard>();
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DBConnectionString"].ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = @"
                        SELECT 
                            Id, Name, Description, Definition, Active, Type, Source, AllowExport
                        FROM 
                            Dashboard
                        WHERE
                            Type = @Type and Active = 1
                        Order By Name
                    ";
                    cmd.Parameters.Add(new SqlParameter("@Type", Type));
                    cmd.CommandType = CommandType.Text;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            ListToReturn.Add(
                                new Dashboard
                                {
                                    Id = reader.IsDBNull(reader.GetOrdinal("Id")) ? 0 : Convert.ToInt32(reader["Id"]),
                                    Name = reader.IsDBNull(reader.GetOrdinal("Name")) ? "" : reader["Name"].ToString(),
                                    Definition = reader.IsDBNull(reader.GetOrdinal("Definition")) ? "" : reader["Definition"].ToString(),
                                    Description = reader.IsDBNull(reader.GetOrdinal("Description")) ? "" : reader["Description"].ToString(),
                                    Type = reader.IsDBNull(reader.GetOrdinal("Type")) ? "" : reader["Type"].ToString(),
                                    Active = reader.IsDBNull(reader.GetOrdinal("Active")) ? true : Convert.ToBoolean(reader["Active"]),
                                    Source =  reader.IsDBNull(reader.GetOrdinal("Source")) ? 1 : Convert.ToInt32(reader["Source"]), 
                                    AllowExport = reader.IsDBNull(reader.GetOrdinal("AllowExport")) ? true : Convert.ToBoolean(reader["AllowExport"])
                                }
                            );
                        }
                    }
                }
            }
            return ListToReturn;
        }

        public List<Dashboard> GetByAll()
        {
            var ListToReturn = new List<Dashboard>();
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DBConnectionString"].ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = @"
                        SELECT 
                            Id, Name, Description, Definition, Active, Type, Source, AllowExport
                        FROM 
                            Dashboard
                        WHERE
                            Active = 1
                        Order By Name
                    ";
                    cmd.CommandType = CommandType.Text;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            ListToReturn.Add(
                                new Dashboard
                                {
                                    Id = reader.IsDBNull(reader.GetOrdinal("Id")) ? 0 : Convert.ToInt32(reader["Id"]),
                                    Name = reader.IsDBNull(reader.GetOrdinal("Name")) ? "" : reader["Name"].ToString(),
                                    Definition = reader.IsDBNull(reader.GetOrdinal("Definition")) ? "" : reader["Definition"].ToString(),
                                    Description = reader.IsDBNull(reader.GetOrdinal("Description")) ? "" : reader["Description"].ToString(),
                                    Type = reader.IsDBNull(reader.GetOrdinal("Type")) ? "" : reader["Type"].ToString(),
                                    Active = reader.IsDBNull(reader.GetOrdinal("Active")) ? true : Convert.ToBoolean(reader["Active"]),
                                    Source =  reader.IsDBNull(reader.GetOrdinal("Source")) ? 1 : Convert.ToInt32(reader["Source"]), 
                                    AllowExport = reader.IsDBNull(reader.GetOrdinal("AllowExport")) ? true : Convert.ToBoolean(reader["AllowExport"])
                                }
                            );
                        }
                    }
                }
            }
            return ListToReturn;
        }

        public Dashboard GetById(int id)
        {
            var ItemToReturn = new Dashboard();
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DBConnectionString"].ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = @"
                        SELECT 
                            Id, Name, Description, Definition, Active, Type, Source, AllowExport
                        FROM 
                            Dashboard
                        WHERE
                            Id = @Id
                    ";
                    cmd.Parameters.Add(new SqlParameter("@Id", id));
                    cmd.CommandType = CommandType.Text;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            ItemToReturn = new Dashboard
                            {
                                Id = reader.IsDBNull(reader.GetOrdinal("Id")) ? 0 : Convert.ToInt32(reader["Id"]),
                                Name = reader.IsDBNull(reader.GetOrdinal("Name")) ? "" : reader["Name"].ToString(),
                                Definition = reader.IsDBNull(reader.GetOrdinal("Definition")) ? "" : reader["Definition"].ToString(),
                                Description = reader.IsDBNull(reader.GetOrdinal("Description")) ? "" : reader["Description"].ToString(),
                                Type = reader.IsDBNull(reader.GetOrdinal("Type")) ? "" : reader["Type"].ToString(),
                                Active = reader.IsDBNull(reader.GetOrdinal("Active")) ? true : Convert.ToBoolean(reader["Active"]),
                                Source = reader.IsDBNull(reader.GetOrdinal("Source")) ? 1 : Convert.ToInt32(reader["Source"]),
                                AllowExport = reader.IsDBNull(reader.GetOrdinal("AllowExport")) ? true : Convert.ToBoolean(reader["AllowExport"])
                            };
                        }
                    }
                }
            }
            return ItemToReturn;
        }

        public void Update(Dashboard CurrentDashboard)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DBConnectionString"].ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = @"
                    UPDATE 
                        Dashboard
                    SET 
                        [Name] = @Name
                        ,[Description] = @Description
                        ,[Definition] = @Definition
                        ,[Active] = @Active
                        ,[Type] = @Type
                        ,Source = @Source
                        ,AllowExport = @AllowExport
                    WHERE 
                        Id = @Id
                    ";
                    cmd.Parameters.Add(new SqlParameter("@Id", CurrentDashboard.Id));
                    cmd.Parameters.Add(new SqlParameter("@Name", CurrentDashboard.Name));
                    cmd.Parameters.Add(new SqlParameter("@Type", CurrentDashboard.Type));
                    cmd.Parameters.Add(new SqlParameter("@Description", CurrentDashboard.Description));
                    cmd.Parameters.Add(new SqlParameter("@Definition", CurrentDashboard.Definition));
                    cmd.Parameters.Add(new SqlParameter("@Active", CurrentDashboard.Active));
                    cmd.Parameters.Add(new SqlParameter("@Source", CurrentDashboard.Source));
                    cmd.Parameters.Add(new SqlParameter("@AllowExport", CurrentDashboard.AllowExport));
                    cmd.CommandType = CommandType.Text;
                    cmd.Connection = con;
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public int Insert(Dashboard CurrentDashboard)
        {
            var newId = 0;
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DBConnectionString"].ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = @"
                    INSERT INTO  Dashboard ([Name],[Description],[Definition],[Type],[Source],[AllowExport])
                    VALUES  (@Name, @Description, @Definition, @Type, @Source, @AllowExport)
                    ";
                    cmd.Parameters.Add(new SqlParameter("@Id", CurrentDashboard.Id));
                    cmd.Parameters.Add(new SqlParameter("@Name", CurrentDashboard.Name));
                    cmd.Parameters.Add(new SqlParameter("@Type", CurrentDashboard.Type));
                    cmd.Parameters.Add(new SqlParameter("@Description", CurrentDashboard.Description));
                    cmd.Parameters.Add(new SqlParameter("@Definition", CurrentDashboard.Definition));
                    cmd.Parameters.Add(new SqlParameter("@Source", CurrentDashboard.Source));
                    cmd.Parameters.Add(new SqlParameter("@AllowExport", CurrentDashboard.AllowExport));
                    cmd.CommandType = CommandType.Text;
                    cmd.Connection = con;
                    cmd.ExecuteNonQuery();
                }
            }
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DBConnectionString"].ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = @"
                        SELECT IDENT_CURRENT('[dbo].[Dashboard]')
                    ";
                    cmd.CommandType = CommandType.Text;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            newId = Convert.ToInt32(reader[0]);
                        }
                    }
                }
            }
            return newId;
        }

        public void Delete(int id)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DBConnectionString"].ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = @"
                    UPDATE 
                        Dashboard
                    SET 
                        [Active] = 0
                    WHERE 
                        Id = @Id
                    ";
                    cmd.Parameters.Add(new SqlParameter("@Id", id));
                    cmd.CommandType = CommandType.Text;
                    cmd.Connection = con;
                    cmd.ExecuteNonQuery();
                }
            }
        }
    }
}
