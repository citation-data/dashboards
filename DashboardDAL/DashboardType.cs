﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DashboardDAL
{
    public class DashboardType
    {

        public string Name { get; set; }

        public List<DashboardType> Get()
        {
            var ListToReturn = new List<DashboardType>();
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DBConnectionString"].ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = @"
                        SELECT 
                            Name
                        FROM 
                            DashboardType
                        WHERE
                            Active = 1
                        Order By Name
                    ";
                    cmd.CommandType = CommandType.Text;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            ListToReturn.Add(
                                new DashboardType
                                {
                                    Name = reader.IsDBNull(reader.GetOrdinal("Name")) ? "" : reader["Name"].ToString()
                                }
                            );
                        }
                    }
                }
            }
            return ListToReturn;
        }
    }
}
