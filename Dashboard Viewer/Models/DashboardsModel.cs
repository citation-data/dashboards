﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Configuration;

namespace Citation.Dashboard.Viewer.Models
{
    public class DashboardsModel
    {
        public List<DashboardFolder> Folders { get; set; }

        public DashboardsModel()
        {
            Folders = new List<DashboardFolder>();
            foreach (var folder in new DashboardDAL.DashboardType().Get())
            {
                Folders.Add(new DashboardFolder(folder.Name));
            }
        }
    }

    public class DashboardFolder
    {
        public List<DashboardDAL.Dashboard> Dashboards { get; set; }
        public string Name { get; set; }

        public DashboardFolder(string FolderName)
        {
            Dashboards = new List<DashboardDAL.Dashboard>();
            Name = FolderName;
            foreach (var dashboardFile in new DashboardDAL.Dashboard().GetByType(FolderName))
            {
                Dashboards.Add(dashboardFile);
            }
        }
    }
}