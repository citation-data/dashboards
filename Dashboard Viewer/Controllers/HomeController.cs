﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DevExpress.Web.Mvc;
using DevExpress.DashboardWeb.Mvc;
using DevExpress.DataAccess.ConnectionParameters;
using System.Configuration;

namespace Citation.Dashboard.Viewer.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index(int? id)
        {
            var model = new DashboardDAL.Dashboard();
            if (id.HasValue)
            {
                model = new DashboardDAL.Dashboard().GetById(id.Value);                
            }
            else
            {
                var home = new DashboardDAL.Dashboard().GetByType("Home");
                if(home.Count > 0)
                {
                    model = home[0];
                }
            }
            return View(model);
        }

        public ActionResult Index2(int id)
        {
            var model = new DashboardDAL.Dashboard().GetById(id);
            return View(model);
        }

        [ValidateInput(false)]
        public ActionResult DashboardViewerPartial(int? id)
        {
            if (id.HasValue)
            {
                Session["DASHBOARDID"] = id;
            }
            return PartialView("_DashboardViewerPartial", new DashboardViewerSettings(Convert.ToInt32(Session["DASHBOARDID"])).Model);
        }
        public FileStreamResult DashboardViewerPartialExport(int? id)
        {
            if(id.HasValue)
            {
                Session["DASHBOARDID"] = id;
            }
            return DevExpress.DashboardWeb.Mvc.DashboardViewerExtension.Export("DashboardViewer", new DashboardViewerSettings(Convert.ToInt32(Session["DASHBOARDID"])).Model);
        }
    }

    public class DashboardViewerSettings
    {
        public int Id;

        public DashboardViewerSettings(int id)
        {
            Id = id;
        }

        public DashboardSourceModel Model
        {
            get
            {
                var model = new DashboardSourceModel();
                model.DashboardId = Id.ToString();
                model.DashboardLoading = (sender, e) =>
                {
                    int DashboardId = 0;
                    if (int.TryParse(e.DashboardId, out DashboardId))
                    {
                        if (DashboardId > 0)
                        {
                            var Dashboard = new DashboardDAL.Dashboard().GetById(DashboardId);
                            e.DashboardXml = Dashboard.Definition;
                        }
                    }
                };
                model.ConfigureDataConnection = (sender, e) =>
                {

                    int DashboardId = 0;
                    int Source = 1;
                    if (int.TryParse(e.DashboardId, out DashboardId))
                    {
                        if (DashboardId > 0)
                        {
                            Source = new DashboardDAL.Dashboard().GetById(DashboardId).Source;
                        }
                    }
                    if(Source.Equals(1)) // Reporting
                    {
                        if(e.ConnectionParameters.GetType().FullName.Equals("DevExpress.DataAccess.ConnectionParameters.CustomStringConnectionParameters"))
                        {
                            e.ConnectionParameters = new MsSqlConnectionParameters("dev2", "SynchHubReporting", "", "", MsSqlAuthorizationType.Windows);
                        }
                        else
                        {
                            MsSqlConnectionParameters parameters = (MsSqlConnectionParameters)e.ConnectionParameters;
                            parameters.AuthorizationType = MsSqlAuthorizationType.Windows;
                            parameters.DatabaseName = "SynchHubReporting";
                            parameters.ServerName = "dev2";
                        }
                    }
                    else if (Source.Equals(2)) // Atlas Production
                    {
                        e.ConnectionParameters = new MsSqlConnectionParameters(ConfigurationManager.AppSettings["ProdServer"], ConfigurationManager.AppSettings["ProdDB"], ConfigurationManager.AppSettings["ProdUserName"], ConfigurationManager.AppSettings["ProdPassword"], MsSqlAuthorizationType.SqlServer);
                    }
                    else if (Source.Equals(3)) // Atlas UAT
                    {
                        e.ConnectionParameters = new MsSqlConnectionParameters(ConfigurationManager.AppSettings["UATServer"], ConfigurationManager.AppSettings["UATDB"], ConfigurationManager.AppSettings["UATUserName"], ConfigurationManager.AppSettings["UATPassword"], MsSqlAuthorizationType.SqlServer);
                    }
                };
                return model;
            }
        }
    }

}