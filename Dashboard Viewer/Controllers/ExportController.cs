﻿using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

namespace Citation.Dashboard.Viewer.Controllers
{
    public class ExportController : Controller
    {
        // GET: Export
        public ActionResult Index(int id)
        {
            var Dashboard = new DashboardDAL.Dashboard().GetById(id);
            if(!Dashboard.AllowExport)
            {
                throw new Exception("Unable to Export Dashboard - Change the value of the export settings if you to export this dashboard to Excel!");
            }
            var doc = new XmlDocument();
            doc.LoadXml(Dashboard.Definition);
            var sql = doc.SelectSingleNode("//Sql");
            var SqlString = sql != null ? sql.InnerText : "";
            if(SqlString.Length.Equals(0))
            {
                return RedirectToAction("Index2", "Home", new { id = id });
            }
            var ConnectionString = ConfigurationManager.ConnectionStrings["Reporting"].ConnectionString;
            if (Dashboard.Source.Equals(2))
            {
                ConnectionString = ConfigurationManager.ConnectionStrings["ProdReadOnly"].ConnectionString;
            }
            else if (Dashboard.Source.Equals(3))
            {
                ConnectionString = ConfigurationManager.ConnectionStrings["AtlasUAT"].ConnectionString;
            }
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(SqlString.Unescape()))
                {
                    using (SqlDataAdapter sda = new SqlDataAdapter())
                    {
                        cmd.Connection = con;
                        sda.SelectCommand = cmd;
                        using (DataTable dt = new DataTable())
                        {
                            sda.Fill(dt);
                            using (XLWorkbook wb = new XLWorkbook())
                            {
                                wb.Worksheets.Add(dt, Dashboard.Name);

                                Response.Clear();
                                Response.Buffer = true;
                                Response.Charset = "";
                                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                                Response.AddHeader("content-disposition", string.Format("attachment;filename={0}.xlsx", Dashboard.Name));
                                using (MemoryStream MyMemoryStream = new MemoryStream())
                                {
                                    wb.SaveAs(MyMemoryStream);
                                    MyMemoryStream.WriteTo(Response.OutputStream);
                                    Response.Flush();
                                    Response.End();
                                }
                            }
                        }
                    }
                }
            }

            //GridView gv = new GridView();
            //var Dashboard = new DashboardDAL.Dashboard().GetById(id);
            //var doc = new XmlDocument();
            //doc.LoadXml(Dashboard.Definition);
            //var sql = doc.SelectSingleNode("//Sql");
            //var SqlString = sql.InnerText;
            //Debug.WriteLine(SqlString.Unescape());
            //Debug.WriteLine(Regex.Unescape(SqlString));
            ////SqlConnection con = new SqlConnection("Data Source=dev2;Initial Catalog=SynchHubReporting;User Id=dev;Password=M@ss1veDev3loper;");
            //SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Reporting"].ConnectionString);
            //con.Open();
            //var Reader = new SqlCommand(SqlString.Unescape(), con);
            //DataTable dt = new DataTable();
            //dt.Load(Reader.ExecuteReader());
            ////SqlDataAdapter da = new SqlDataAdapter(SqlString.Unescape(), con);
            ////da.SelectCommand.
            ////da.Fill(dt);
            //con.Close();
            //gv.DataSource = dt;
            //gv.DataBind();
            //Response.ClearContent();
            //Response.Buffer = true;
            ////Response.AddHeader("content-disposition", "attachment; filename=" + Dashboard.Name + ".xls");
            ////Response.ContentType = "application/ms-excel";
            //Response.ContentType = "application/application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            //Response.AppendHeader("content-disposition", "attachment; filename=" + Dashboard.Name + ".xlsx");
            //Response.Charset = "";
            //StringWriter sw = new StringWriter();
            //HtmlTextWriter htw = new HtmlTextWriter(sw);
            //gv.RenderControl(htw);
            //Response.Output.Write(sw.ToString());
            //Response.Flush();
            //Response.End();
            return RedirectToAction("Index", "Home", new { id = id });
        }
    }

    public static class StringUnescape
    {
        public static string Unescape(this string txt)
        {
            if (string.IsNullOrEmpty(txt)) { return txt; }
            StringBuilder retval = new StringBuilder(txt.Length);
            for (int ix = 0; ix < txt.Length; )
            {
                int jx = txt.IndexOf('\\', ix);
                if (jx < 0 || jx == txt.Length - 1) jx = txt.Length;
                retval.Append(txt, ix, jx - ix);
                if (jx >= txt.Length) break;
                switch (txt[jx + 1])
                {
                    case 'n': retval.Append('\n'); break;  // Line feed
                    case 'r': retval.Append('\r'); break;  // Carriage return
                    case 't': retval.Append('\t'); break;  // Tab
                    case '\\': retval.Append('\\'); break; // Don't escape
                    default:                                 // Unrecognized, copy as-is
                        retval.Append('\\').Append(txt[jx + 1]); break;
                }
                ix = jx + 2;
            }
            return retval.ToString();
        }
    }
}