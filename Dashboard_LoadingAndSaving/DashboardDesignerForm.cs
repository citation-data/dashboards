﻿using System.Windows.Forms;
using DevExpress.XtraBars.Ribbon;
using System.Configuration;
using System;
using DashboardDAL;
using System.IO;

namespace Dashboard_LoadingAndSaving {
    public partial class DashboardDesignerForm : RibbonForm {
        public int CurrentDashboardId = 0;
        public DashboardDesignerForm() {
            InitializeComponent();
            //dashboardDesigner1.CreateRibbon();
            //dashboardDesigner1.CreateBars();
            fileSaveAsBarItem1.Enabled = false;
            fileSaveBarItem1.Enabled = false;
            ribbonControl1.ShowApplicationButton = DevExpress.Utils.DefaultBoolean.False;
            openFileDialog1.CheckFileExists = true;
            openFileDialog1.DefaultExt = "xml";
            openFileDialog1.Filter = "Dashboard Files|*.xml";
            openFileDialog1.InitialDirectory = ConfigurationManager.AppSettings["DashboardsLocation"];
        }

        // Handles the DashboardSaving event.
        private void dashboardDesigner1_DashboardSaving(object sender, DevExpress.DashboardWin.DashboardSavingEventArgs e) {
            var stream = new MemoryStream();
            dashboardDesigner1.Dashboard.SaveToXml(stream);
            stream.Position = 0;
            StreamReader reader = new StreamReader(stream);
            var XMLDefinition = reader.ReadToEnd();
            // Determines whether the user has called the Save command.
            if (e.Command == DevExpress.DashboardWin.DashboardSaveCommand.SaveAs || CurrentDashboardId.Equals(0))
            {
                var SaveForm = new SaveDashboard();
                SaveForm.Defintion = XMLDefinition;
                var Result = SaveForm.ShowDialog();
                CurrentDashboardId = SaveForm.DashboardId;
            }
            else
            {
                var Dashboard = new Dashboard().GetById(CurrentDashboardId);
                Dashboard.Definition = XMLDefinition;
                new Dashboard().Update(Dashboard);
            }
            e.Handled = true;
            e.Saved = true;
        }

        private void dashboardDesigner1_DashboardOpening(object sender, DevExpress.DashboardWin.DashboardOpeningEventArgs e) {
            if(MessageBox.Show("Would you like to load dashboard from the file system?", "Load Dashboard", 
                MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == System.Windows.Forms.DialogResult.Yes)
            {
                openFileDialog1.ShowDialog();
            }
            else
            {
                var OpenForm = new OpenDashboard();
                if(OpenForm.ShowDialog().Equals(DialogResult.OK))
                {
                    CurrentDashboardId = OpenForm.DashboardId;
                    var Dashboard = new Dashboard().GetById(CurrentDashboardId);
                    MemoryStream stream = new MemoryStream();
                    StreamWriter writer = new StreamWriter(stream);
                    writer.Write(Dashboard.Definition);
                    writer.Flush();
                    stream.Position = 0;
                    dashboardDesigner1.LoadDashboard(stream);
                }
            }
            fileSaveAsBarItem1.Enabled = true;
            fileSaveBarItem1.Enabled = true;
            e.Handled = true;
        }

        private void openFileDialog1_FileOk(object sender, System.ComponentModel.CancelEventArgs e)
        {
            dashboardDesigner1.LoadDashboard(openFileDialog1.FileName);
            CurrentDashboardId = 0;
        }
    }
}
