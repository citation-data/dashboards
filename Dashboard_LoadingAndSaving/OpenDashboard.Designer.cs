﻿namespace Dashboard_LoadingAndSaving
{
    partial class OpenDashboard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ListOfDashboards = new System.Windows.Forms.ListView();
            this.SelectButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // ListOfDashboards
            // 
            this.ListOfDashboards.FullRowSelect = true;
            this.ListOfDashboards.GridLines = true;
            this.ListOfDashboards.Location = new System.Drawing.Point(12, 12);
            this.ListOfDashboards.MultiSelect = false;
            this.ListOfDashboards.Name = "ListOfDashboards";
            this.ListOfDashboards.Size = new System.Drawing.Size(561, 248);
            this.ListOfDashboards.TabIndex = 0;
            this.ListOfDashboards.UseCompatibleStateImageBehavior = false;
            this.ListOfDashboards.View = System.Windows.Forms.View.SmallIcon;
            this.ListOfDashboards.SelectedIndexChanged += new System.EventHandler(this.ListOfDashboards_SelectedIndexChanged);
            // 
            // SelectButton
            // 
            this.SelectButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.SelectButton.Enabled = false;
            this.SelectButton.Location = new System.Drawing.Point(482, 273);
            this.SelectButton.Name = "SelectButton";
            this.SelectButton.Size = new System.Drawing.Size(91, 34);
            this.SelectButton.TabIndex = 1;
            this.SelectButton.Text = "Select";
            this.SelectButton.UseVisualStyleBackColor = true;
            this.SelectButton.Click += new System.EventHandler(this.SelectButton_Click);
            // 
            // OpenDashboard
            // 
            this.AcceptButton = this.SelectButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(585, 318);
            this.Controls.Add(this.SelectButton);
            this.Controls.Add(this.ListOfDashboards);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "OpenDashboard";
            this.ShowIcon = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Select a dashboard to open";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.OpenDashboard_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView ListOfDashboards;
        private System.Windows.Forms.Button SelectButton;
    }
}