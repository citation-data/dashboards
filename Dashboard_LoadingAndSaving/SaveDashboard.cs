﻿using System;
using System.Linq;
using DashboardDAL;
using System.Windows.Forms;
using System.Collections.Generic;

namespace Dashboard_LoadingAndSaving
{
    public partial class SaveDashboard : Form
    {
        private int SelectedDashboardId;
        private string XMLDefinition;

        public SaveDashboard()
        {
            InitializeComponent();
            SelectedDashboardId = 0;
        }

        public int DashboardId
        {
            get
            {
                return SelectedDashboardId;
            }
            set
            {
                SelectedDashboardId = value;
            }
        }

        public string Defintion
        {
            get
            {
                return XMLDefinition;
            }
            set
            {
                XMLDefinition = value;
            }
        }

        private void SelectButton_Click(object sender, EventArgs e)
        {
            var Dashboard = new Dashboard().GetById(SelectedDashboardId);
            Dashboard.Name = NameTextBox.Text;
            Dashboard.Description = DescriptionTextBox.Text;
            Dashboard.Definition = XMLDefinition;
            Dashboard.Type = typesDDL.Text;
            Dashboard.Source = SourceDDL.SelectedIndex;
            Dashboard.AllowExport = AllowExportCheckBox.Checked;
            if(SelectedDashboardId > 0)
            {
                new Dashboard().Update(Dashboard);
            }
            else
            {
                SelectedDashboardId = new Dashboard().Insert(Dashboard);
            }
            this.Close();
        }

        private void SaveDashboard_Load(object sender, EventArgs e)
        {
            var Dashboard = new Dashboard().GetById(SelectedDashboardId);
            NameTextBox.Text = Dashboard.Name;
            DescriptionTextBox.Text = Dashboard.Description;
            typesDDL.Items.Clear();
            foreach (var Type in new DashboardType().Get())
            {
                typesDDL.Items.Add(Type.Name);
                if(Type.Name.Equals(Dashboard.Type))
                {
                    typesDDL.SelectedIndex = (typesDDL.Items.Count - 1);
                }
            }
            SourceDDL.Items.Clear();
            SourceDDL.Items.Insert(0, "-- Please Select --");
            SourceDDL.Items.Insert(1, "Reporting");
            SourceDDL.Items.Insert(2, "Altas Production");
            SourceDDL.Items.Insert(3, "Atlas UAT");
            SourceDDL.SelectedIndex = Dashboard.Source;
            AllowExportCheckBox.Checked = Dashboard.AllowExport;
        }

        private void typesDDL_SelectedIndexChanged(object sender, EventArgs e)
        {
            EnableDisableSaveButton();
        }

        private void NameTextBox_TextChanged(object sender, EventArgs e)
        {
            EnableDisableSaveButton();
        }

        private void EnableDisableSaveButton()
        {
            if (NameTextBox.TextLength > 0 && DescriptionTextBox.TextLength > 0 && typesDDL.SelectedIndex >= 0 && SourceDDL.SelectedIndex >= 0)
            {
                SelectButton.Enabled = true;
            }
            else
            {
                SelectButton.Enabled = false;
            }
        }

        private void DescriptionTextBox_TextChanged(object sender, EventArgs e)
        {
            EnableDisableSaveButton();
        }

        private void SourceDDL_SelectedIndexChanged(object sender, EventArgs e)
        {
            EnableDisableSaveButton();
        }

        private void AllowExportCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            EnableDisableSaveButton();
        }
    }
}
