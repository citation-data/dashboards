﻿using DashboardDAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Dashboard_LoadingAndSaving
{
    public partial class OpenDashboard : Form
    {
        private int SelectedDashboardId;

        public OpenDashboard()
        {
            InitializeComponent();
            SelectedDashboardId = 0;
        }

        public int DashboardId
        {
            get
            {
                return SelectedDashboardId;
            }
        }

        private void OpenDashboard_Load(object sender, EventArgs e)
        {
            ListOfDashboards.Items.Clear();
            SelectButton.Enabled = false;
            foreach(var DashboardType in new DashboardType().Get())
            {
                var ListGroup = ListOfDashboards.Groups.Add(DashboardType.Name, DashboardType.Name);
                foreach (var Dashboard in new Dashboard().GetByType(DashboardType.Name))
	            {
                    ListOfDashboards.Items.Add(new ListViewItem { Group = ListGroup, Name = Dashboard.Name, Tag = Dashboard.Id.ToString(), Text = Dashboard.Name });
	            }
            }
        }

        private void ListOfDashboards_SelectedIndexChanged(object sender, EventArgs e)
        {
            SelectButton.Enabled = true;
        }

        private void SelectButton_Click(object sender, EventArgs e)
        {
            SelectedDashboardId = Convert.ToInt32(ListOfDashboards.SelectedItems[0].Tag);
            this.Close();
        }
    }
}
