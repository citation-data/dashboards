﻿namespace Dashboard_LoadingAndSaving
{
    partial class SaveDashboard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.typesDDL = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.NameTextBox = new System.Windows.Forms.TextBox();
            this.DescriptionTextBox = new System.Windows.Forms.TextBox();
            this.SelectButton = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.SourceDDL = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.AllowExportCheckBox = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // typesDDL
            // 
            this.typesDDL.FormattingEnabled = true;
            this.typesDDL.Location = new System.Drawing.Point(256, 22);
            this.typesDDL.Name = "typesDDL";
            this.typesDDL.Size = new System.Drawing.Size(331, 21);
            this.typesDDL.TabIndex = 0;
            this.typesDDL.SelectedIndexChanged += new System.EventHandler(this.typesDDL_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(32, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(31, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Type";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(32, 61);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Name";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(32, 103);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Description";
            // 
            // NameTextBox
            // 
            this.NameTextBox.Location = new System.Drawing.Point(256, 58);
            this.NameTextBox.Name = "NameTextBox";
            this.NameTextBox.Size = new System.Drawing.Size(331, 20);
            this.NameTextBox.TabIndex = 4;
            this.NameTextBox.TextChanged += new System.EventHandler(this.NameTextBox_TextChanged);
            // 
            // DescriptionTextBox
            // 
            this.DescriptionTextBox.Location = new System.Drawing.Point(256, 96);
            this.DescriptionTextBox.Name = "DescriptionTextBox";
            this.DescriptionTextBox.Size = new System.Drawing.Size(331, 20);
            this.DescriptionTextBox.TabIndex = 5;
            this.DescriptionTextBox.TextChanged += new System.EventHandler(this.DescriptionTextBox_TextChanged);
            // 
            // SelectButton
            // 
            this.SelectButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.SelectButton.Enabled = false;
            this.SelectButton.Location = new System.Drawing.Point(496, 202);
            this.SelectButton.Name = "SelectButton";
            this.SelectButton.Size = new System.Drawing.Size(91, 34);
            this.SelectButton.TabIndex = 6;
            this.SelectButton.Text = "Save";
            this.SelectButton.UseVisualStyleBackColor = true;
            this.SelectButton.Click += new System.EventHandler(this.SelectButton_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(32, 137);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Source";
            // 
            // SourceDDL
            // 
            this.SourceDDL.FormattingEnabled = true;
            this.SourceDDL.Items.AddRange(new object[] {
            "Reporting = 1",
            "Atlas Production = 2",
            "Altas UAT = 3"});
            this.SourceDDL.Location = new System.Drawing.Point(256, 137);
            this.SourceDDL.Name = "SourceDDL";
            this.SourceDDL.Size = new System.Drawing.Size(331, 21);
            this.SourceDDL.TabIndex = 7;
            this.SourceDDL.SelectedIndexChanged += new System.EventHandler(this.SourceDDL_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(32, 176);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(110, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Allow Export To Excel";
            // 
            // AllowExportCheckBox
            // 
            this.AllowExportCheckBox.AutoSize = true;
            this.AllowExportCheckBox.Checked = true;
            this.AllowExportCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.AllowExportCheckBox.Location = new System.Drawing.Point(258, 176);
            this.AllowExportCheckBox.Name = "AllowExportCheckBox";
            this.AllowExportCheckBox.Size = new System.Drawing.Size(15, 14);
            this.AllowExportCheckBox.TabIndex = 10;
            this.AllowExportCheckBox.UseVisualStyleBackColor = true;
            this.AllowExportCheckBox.CheckedChanged += new System.EventHandler(this.AllowExportCheckBox_CheckedChanged);
            // 
            // SaveDashboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(602, 248);
            this.Controls.Add(this.AllowExportCheckBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.SourceDDL);
            this.Controls.Add(this.SelectButton);
            this.Controls.Add(this.DescriptionTextBox);
            this.Controls.Add(this.NameTextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.typesDDL);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SaveDashboard";
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Save Current Dashboard";
            this.Load += new System.EventHandler(this.SaveDashboard_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox typesDDL;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox NameTextBox;
        private System.Windows.Forms.TextBox DescriptionTextBox;
        private System.Windows.Forms.Button SelectButton;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox SourceDDL;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox AllowExportCheckBox;
    }
}